/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicioslogic1joseignacionavassanz;

/**
 *
 * @author Jose Ignacio Navas Sanz CURSO: 1º DAM ASIGNATURA: PROGRAMACIÓN
 *
 * Ejercicios CodingBat - Logic1 Serie de ejercicios de la Web CodingBat
 * consistentes en programas cortos en los que debemos elaborar una cadena de
 * pruebas
 *
 * Cada programa es diferente. En este caso, son 30 programas en lenguaje Java.
 *
 * Algunos ya los teníamos realizados de clase, pero el objetivo es aprender a
 * programar de un modo correcto y eficiente por lo que he realizado todos y
 * cada uno de ellos sin mirar los anteriores.
 *
 * Finalizado -- 31 Enero 2016
 */
public class EjerciciosLogic1JoseIgnacioNavasSanz {

    /*
    Ejercicio 1 - CodingBat Logic1 - cigarParty
    Unas ardillas se reúnen para hacer fiestas
    Las ardillas fuman durante las fiestas.
    Para que esa fiesta se pueda hacer, tiene que
    haber al menos 40 cigarros y como máximo 60,
    a no ser que sea fin de semana, en cuyo caso
    el máximo de cigarros les da igual.
    Recibe dos parámetros. Un entero del número
    de cigarros y un booleano de si es fin de
    semana o no
     */
    public boolean cigarParty(int cigars, boolean isWeekend) {
        /*
        Si es fin de semana y hay al menos 40
        cigarros la fiesta se celebra exitosamente
         */
        if (isWeekend && cigars >= 40) {
            return true;
        }
        /*
        Si hay entre 40 y 60 cigarros la fiesta
        también se celebra, independientemente
        de que sea o no fin de semana
         */
        if (cigars >= 40 && cigars <= 60) {
            return true;
        } /*
        En cualquier otro caso, no hay fiesta
         */ else {
            return false;
        }
    }

    /*
    Ejercicio 2 - CodingBat Logic1 - dateFashion
    Tú y una cita os dirigis a un restaurante.
    En función de la calidad de vuestra vestimenta
    os darán la mesa (el programa devuelve 2), no
    obtendreis la mesa (devolverá 0), o quizá os
    den mesa (devolverá 1). Para evaluar la calidad
    de vuestra vestimenta se hace una escala con
    valores del 0 al 10 donde 10 es el resultado
    más estiloso y 0 el menos.
    Hay una serie de normas. Si alguno de los dos
    puntúa con un 8 o superior os darán la mesa a
    no ser que el otro acuda con 2 o menos que 
    en ese caso no obtendréis la mesa. En cualquier
    otra combinación puede que os den la mesa (1).
     */
    public int dateFashion(int you, int date) {
        /*
        Si ninguno de los dos va de más de 8 o
        alguno de los dos va de 8 o más pero el
        otro va de 2 o menos no os darán la mesa
        y el programa devolverá 0
         */
        if (date <= 2 && you <= 7 || date <= 7 && you <= 2
                || date >= 8 && you <= 2 || you >= 8 && date <= 2) {
            return 0;
        }
        /*
        Si cualquiera de los dos va de más de 8 y el otro va
        de 3 o más, os darán seguro la mesa. El programa
        devolverá 2 en este caso.
         */
        if (you >= 8 && date >= 3 || date >= 8 && you >= 3) {
            return 2;
        } /*
        En cualquier otro caso, el programa devolverá 1
         */ else {
            return 1;
        }
    }

    /*
    Ejercicio 3 - CodingBat Logic1 - squirrelPlay
    Las ardillas de Palo Alto, California, se pasan
    la mayoría del día jugando, eso sí, mientras
    la temperatura esté entre 60 y 90 grados, a no
    ser que sea verano, donde la temperatura máxima
    puede aumentar hasta los 100 grados.
    El programa recibe dos parámetros, un entero
    con la temperatura y un booleano que indica si es
    o no es verano, y devuelve un booleano que indica
    si las ardillas juegan o no.
     */
    public boolean squirrelPlay(int temp, boolean isSummer) {
        /*
        Si es verano y la temperatura no excede de
        los 100 grados, devolverá true. La temperatura
        mínima para que las ardillas jueguen es de 60
        grados. Si hace menos de 60 grados no jugarán
         */
        if (isSummer && temp >= 60 && temp <= 100) {
            return true;
        }
        /*
        Si la temperatura está entre 60 y 90 grados,
        las ardillas jugarán independientemente de 
        que sea Verano o no.
         */
        if (temp >= 60 && temp <= 90) {
            return true;
        } /*
        En cualquier otro caso, no jugarán
         */ else {
            return false;
        }
    }

    /*
    Ejercicio 4 - CodingBat Logic1 - caughtSpeeding
    Un policía te para por exceso de velocidad.
    Si ese día es tu cumpleaños, la velocidad puede
    incrementarse hasta en 5 km.
    En función de la velocidad a la que te detenga
    no te pondrá multa (el programa devuelve 0), te
    pondrá una pequeña multa (el programa devuelve 1)
    o te pondrá una multa grande (el programa devuelve 2)
    
    El programa recibe dos parámetros, un entero que
    indica la velocidad y un booleano que especifica 
    si es o no es tu cumpleaños.
    Devuelve un entero en función del resultado de la
    multa que te pone el policía.
     */
    public int caughtSpeeding(int speed, boolean isBirthday) {
        /*
        Si es tu cumpleaños, la velocidad puede
        ser de hasta 5km más de lo establecido
         */
        if (isBirthday) {
            speed = speed - 5;
        }
        /*
        Si vas a menos de 60km/h no habrá multa
         */
        if (speed <= 60) {
            return 0;
        }
        /*
        Si circulabas entre 61km/h y 80km/h, la
        multa será pequeña y el programa devolverá
        un 1
         */
        if (speed >= 61 && speed <= 80) {
            return 1;
        } /*
        En cualquier otro caso, la multa será
        la superior y el programa devolverá 2
         */ else {
            return 2;
        }
    }

    /*
    Ejercicio 5 - CodingBat Logic1 - sortaSum 
    Dados dos enteros A y B, devuelve su suma
    a no ser que la suma esté en el intervalo
    entre 10 y 19, ambos incluidos que está 
    prohibido y devolverá 20 en ese caso.
    El programa devuelve un entero con el valor
    de la suma.
     */
    public int sortaSum(int a, int b) {
        /*
        Si la suma está entre 10 y 19, al
        estar prohibidos devuelve 20.
         */
        if (a + b >= 10 && a + b <= 19) {
            return 20;
        } /*
        En cualquier otro caso, devuelve 
        la suma de los dos números.
         */ else {
            return a + b;
        }
    }

    /*
    Ejercicio 6 - CodingBat Logic1 - alarmClock
    Dado un entero que indica el día de la semana
    en el que nos encontramos, siendo 0 el Domingo
    y 6 el Sábado y un booleano que indica si estamos
    o no en vacaciones, devuelve un String en función
    de la hora a la que debe sonar la alarma.
    Si es un día entre semana y no estamos de vacaciones,
    la alarma sonará a las 7:00 si es fin de semana y no
    hay vacaciones, la alarma sonará a las 10:00
    
    En caso de estar de vacaciones, la alarma sonará a 
    las 10:00 de Lunes a Viernes y los fines de semana
    estará apagada (devuelve el valor "off").
     */
    public String alarmClock(int day, boolean vacation) {
        /*
        Si estamos de vacaciones y es sábado o es 
        domingo, la alarma estará apagada ("off")
         */
        if (vacation && day == 6 || day == 0 && vacation) {
            return "off";
        }
        /*
        Si estamos de vacaciones pero es Lunes/Martes
        Miercoles/Jueves/Viernes la alarmá sonará a 
        las 10:00 ("10:00")
         */
        if (vacation && day >= 1 && day <= 5) {
            return "10:00";
        }
        /*
        Si estamos en un día de la semana de L a V
        y no hay vacaciones la alarma sonará a las
        7:00 ("7:00")
         */
        if (day >= 1 && day <= 5 && !vacation) {
            return "7:00";
        } /*
        En cualquier otro caso devolverá "10:00"
         */ else {
            return "10:00";
        }
    }

    /*
    Ejercicio 7 - CodingBat Logic1 - love6
    Dados dos enteros A y B, devuelve true
    si alguno de los dos es igual a 6. Si
    ninguno de los 2 es 6 pero bien el valor
    absoluto (Math.abs()) de su suma o resta
    es 6, también devolverá true.
    En cualquier otro caso, devolverá false.
     */
    public boolean love6(int a, int b) {
        /*
        Si A o B es 6 devolverá true
         */
        if (a == 6 || b == 6) {
            return true;
        }
        /*
        Si la suma de A+B o de B+A o bien, la
        resta de A-B o B-A es igual a 6 devolverá
        true. Con Math.abs no me daba bien el 
        problema
         */
        if (a + b == 6 || b + a == 6 || a - b == 6 || b - a == 6) {
            return true;
        } /*
        En cualquier otro caso, devolverá false.
         */ else {
            return false;
        }
    }

    /*
    Ejercicio 8 - CodingBat Logic1 - in1To10
    Dado un entero en un rango entre 1 y 10
    y un boolenao outsideMode devuelve true
    si el número está entre 1 y 10, a no ser
    que este en outSideMode que devolverá
    true si el número es menor que 1 o bien,
    el número es mayor que 10. 
    
    En cualquier otro caso, devolverá false
     */
    public boolean in1To10(int n, boolean outsideMode) {
        /*
        Si está en outSideMode y el número es menor o igual a 1
        o bien el número es <=10 devolverá true
         */
        if (outsideMode && n <= 1 || outsideMode && n >= 10) {
            return true;
        }
        /*
        Si el número está entre 1 y 10 y no estamos
        en outsideMode, devolverá true
         */
        if (n >= 1 && n <= 10 && !outsideMode) {
            return true;
        } /*
        En cualquier otro caso, devolverá false
         */ else {
            return false;
        }
    }

    /*
    Ejercicio 9 - CodingBat Logic1 - specialEleven
    Dado un entero N, devolverá true si este es
    múltiplo de 11 o 1 más a un múltiplo de 11.
    En cualquier otro caso, devolverá false
     */
    public boolean specialEleven(int n) {
        /*
        Si el resto de la división es 0 es múltiplo de 11
        Si el resto de la división es 1 es múltiplo de 11 + 1
        Por tanto devolverá true en ambos casos.
         */
        if (n % 11 == 0 || n % 11 == 1) {
            return true;
        } /*
        Para todo lo demás (MASTERCARD) no jajaja devuelve 0
         */ else {
            return false;
        }
    }

    /*
    Ejercicio 10 - CodingBat Logic1 - more20
    Dado un entero N devolverá true sólamente
    si ese entero es 1 o 2 más de un múltiplo
    de 20. Si no, devolverá false
     */
    public boolean more20(int n) {
        /*
        Si el resto es 1 o 2, será uno o dos
        más de un múltiplo de 20 y por tanto
        el programa devolverá true
         */
        if (n % 20 == 1 || n % 20 == 2) {
            return true;
        } /*
        En cualquier otro caso, false
         */ else {
            return false;
        }
    }

    /*
    Ejercicio 11 - CodingBat Logic1 - old35
    Dado un entero N devuelve verdadero si
    es divisible entre 3 o bien entre 5 pero
    en ningún caso entre los dos
     */
    public boolean old35(int n) {
        /*
        Si N es divisible entre 3 y no entre 5
        devolverá verdadero
         */
        if (n % 3 == 0 && n % 5 != 0) {
            return true;
        }
        /*
        Del mismo modo, si N es divisible entre
        5 y no entre 3 devolverá verdadero
         */
        if (n % 5 == 0 && n % 3 != 0) {
            return true;
        } /*
        En cualquier otro caso, devolverá false
         */ else {
            return false;
        }
    }

    /*
    Ejercicio 12 - CodingBat Logic1 - less20
    Devuelve verdadero si el número no negativo
    es 1 o 2 menos que un múltiplo de 20
     */
    public boolean less20(int n) {
        /*
        Si el resto de la división es
        18 o 19, el número será 1 o 2 menos
        que un múltiplo de 20 y por tanto,
        devolverá verdadero
         */
        if (n % 20 <= 19 && n % 20 >= 18) {
            return true;
        } /*
        En cualquier otro caso, devolverá
        falso.
         */ else {
            return false;
        }
    }

    /*
    Ejercicio 13 - CodingBat Logic1 - nearTen
    Dado un entero no negativo, devuelve
    verdadero si el número esta como máximo
    a dos dígitos de diferencia de un 
    múltiplo de 10
     */
    public boolean nearTen(int num) {
        /*
        Si el resto es 1, 2, 8 o 9, el número
        estará como máximo a 2 de un múltiplo
        de 10 y devolverá verdadero
         */
        if (num % 10 >= 1 && num % 10 <= 2 || num % 10 >= 8 && num % 10 <= 9) {
            return true;
        }
        if (num % 10 == 0) {
            return true;
        } /*
        En cualquier otro caso, devolverá falso
         */ else {
            return false;
        }
    }

    /*
    Ejercicio 14 - CodingBat Logic1 -teenSum
    Dados dos enteros A y B, devuelve su suma.
    Con una excepción, si A, B o ambos están en 
    el rango entre 13 y 19 devolverá 19.
     */
    public int teenSum(int a, int b) {
        /*
        Si A se encuentra entre el rango
        de 13 a 19, devuelve 19
         */
        if (a >= 13 && a <= 19) {
            return 19;
        }
        /*
        Igual pasa con B, si está entre 13
        y 19 el programa devolverá 19
         */
        if (b >= 13 && b <= 19) {
            return 19;
        } /*
        En cualquier otro caso, devolverá la suma
         */ else {
            return a + b;
        }
    }

    /*
    Ejercicio 15 - CodingBat Logic1 -answerCell
    Programa que especifica cuando contestamos al
    teléfono. Normalmente respondes, a excepción
    de que sea por la mañana, que solamente respondes
    si te llama tu madre. Además si estás durmiendo,
    nunca respondes.
     */
    public boolean answerCell(boolean isMorning, boolean isMom, boolean isAsleep) {
        /*
        Si estás dormido, no respondes
         */
        if (isAsleep) {
            return false;
        }
        /*
        Si es por la mañana y no es tu
        madre la que llama no respondes
         */
        if (isMorning && !isMom) {
            return false;
        } /*
        En cualquier otro caso, respondes
         */ else {
            return true;
        }
    }

    /*
    Ejercicio 16 - CodingBat Logic1 - teaParty
    Organizamos una fiesta con montones de te y
    montones de caramelos. En función de la cantidad
    de te o caramelos que haya la fiesta será mala (0),
    buena (1) o increíble (2). Si ni el te ni los
    caramelos llegan a 5 unidades, la fiesta será mala 
    y devolverá 0. Si hay más de 5 la fiesta ya será
    buena y devolverá 1. Si hay el doble de te que de
    caramelos o viceversa, la fiesta será genial y
    devolverá un 2.
     */
    public int teaParty(int tea, int candy) {
        /*
        Si ni el te ni los caramelos llegan a 5
        la fiesta será mala. Devolveremos 0
         */
        if (candy < 5 || tea < 5) {
            return 0;
        }
        /*
        Si hay el doble de te que de caramelos o
        viceversa, la fiesta será genial y tendremos
        que devolver el 2
         */
        if (candy >= 2 * tea || tea >= 2 * candy) {
            return 2;
        } /*
        En cualquier otro caso, devolveremos 1
         */ else {
            return 1;
        }
    }

    /*
    Ejercicio 17 - CodingBat Logic1 - fizzString
    Dado un String devolverá Fizz si empieza por f
    o Buzz si termina con B. En caso de empezar
    con F y terminar en B devolverá FizzBuzz
     */
    public String fizzString(String str) {
        /*
        Si empieza con F y acaba en B devuelve FizzBuzz
         */
        if (str.startsWith("f") && str.endsWith("b")) {
            return "FizzBuzz";
        }
        /*
        Si sólamente empieza con F devuelve Fizz
         */
        if (str.startsWith("f")) {
            return "Fizz";
        }
        /*
        Si acaba en B devuelve "Buzz"
         */
        if (str.endsWith("b")) {
            return "Buzz";
        } /*
        En cualquier otro caso, que no se contempla
        el programa devolverá el mismo String recibido
         */ else {
            return str;
        }
    }

    /*
    Ejercicio 18 - CodingBat Logic1 - fizzString2
    Dado un entero N, devuelve un String compuesto
    del número recibido por parámetro seguido del
    símbolo "!". A no ser que el número sea divisible
    entre 3 que devolverá "Fizz!" Si es divisible entre
    5 devolverá "Buzz!" y si es divisible entre los dos
    devolverá "FizzBuzz!
     */
    public String fizzString2(int n) {
        /*
        Si es divisible tanto por 3 como por 5,
        devolverá "FizzBuzz!"
         */
        if (n % 3 == 0 && n % 5 == 0) {
            return "FizzBuzz!";
        }
        /*
        Si sólamente es divisible por 3 devolverá "Fizz!"
         */
        if (n % 3 == 0) {
            return "Fizz!";
        }

        /*
        Si sólamente es divisible por 5 devolverá "Buzz!"
         */
        if (n % 5 == 0) {
            return "Buzz!";
        } /*
        En el resto de casos, devolverá el 
        número seguido del símbolo "!"
         */ else {
            return n + "!";
        }
    }

    /*
    Ejercicio 19 - CodingBat Logic1 - twoAsOne
    Dados tres enteros A B y C devuelve verdadero
    si es posible combinar dos de ellos para obtener
    el otro. Si no, devolverá falso.
     */
    public boolean twoAsOne(int a, int b, int c) {
        /*
        Si A+B es igual a C devolverá verdadero
         */
        if (a + b == c) {
            return true;
        }
        /*
        Si B+C es igual a A devolverá verdadero
         */
        if (b + c == a) {
            return true;
        }
        /*
        Si C+A es igual a B devolverá verdadero
         */
        if (c + a == b) {
            return true;
        } /*
        Al no haber más combinaciones posibles
        en todos los casos restantes debermeos
        devolver falso.
         */ else {
            return false;
        }
    }

    /*
    Ejercicio 20 - CodingBat Logic1 - inOrder
    Dados tres enteros A,B y C devuelve verdadero
    sólamente si están ordenados, es decir, que 
    A sea menor que B y B a su vez sea menor que C.
    A no ser que el booleano bOk sea cierto,
    en cuyo caso no es necesario que B sea mayor que
    A pero si que C sea mayor que B.
     */
    public boolean inOrder(int a, int b, int c, boolean bOk) {
        /*
        Si bOk es verdadero y C es mayor que B y B es igual
        que A, mayor o menor, devolverá verdadero
         */
        if (bOk && a >= b && b < c || bOk && a < b && b < c) {
            return true;
        }
        /*
        Si bOk no es verdadero, y A menor que B y B menor
        que C, devolverá verdadero también
         */
        if (!bOk && a < b && b < c) {
            return true;
        } /*
        Para los demás casos, devolverá falso
         */ else {
            return false;
        }
    }

    /*
    Ejercicio 21 - CodingBat Logic1 - inOrderEqual
    Dados tres enteros A, B y C devolverá verdadero
    sólamente si están estrictamente en orden creciente
    con la excepcion de que equalOk sea cierto, en cuyo
    caso A y B pueden ser iguales mientras C sea mayor
    que ambos o bien, ser A, B y C iguales.
     */
    public boolean inOrderEqual(int a, int b, int c, boolean equalOk) {
        /*
        Si equalOk es verdadero y A y B son iguales e inferiores que C
        o bien, A, B y C son iguales deberemos devolver verdadero
         */
        if (equalOk && a == b && b < c || equalOk && a == b && b == c
                || equalOk && a < b && b == c) {
            return true;
        }
        /*
        Del mismo modo, si A B y C están en orden creciente
        devolveremos verdadero también
         */
        if (a < b && b < c) {
            return true;
        } /*
        Para cualquier otro caso, deberemos devolver false
         */ else {
            return false;
        }
    }

    /*
    Ejercicio 22 - CodingBat Logic1 - lastDigit
    Dados tres enteros A B y C devuelve verdadero en el único
    caso de que al menos dos tengan el mismo dígito a la derecha
    En caso contrario devolverá falso.
    Para hallar el dígito de la derecha utilizamos el resto de
    la división entera. (%)
     */
    public boolean lastDigit(int a, int b, int c) {
        /*
        Si el resto de A es igual al de B
        O bien, el de B es igual al de C
        o bien, el de C es igual al de A
        devolveremos verdadero
         */
        if (a % 10 == b % 10 || b % 10 == c % 10 || a % 10 == c % 10) {
            return true;
        } /*
        En cualquier otro caso, devolveremos false
         */ else {
            return false;
        }
    }

    /*
    Ejercicio 23 - CodingBat Logic1 - lessBy10
    Dados tres enteros A, B y C, devuelve verdadero
    si uno de ellos es 10 o más que otro. 
     */
    public boolean lessBy10(int a, int b, int c) {
        /*
        Si A difiere en 10 o más de B
        O B difiere en 10 o más de C
        O C difiere en 10 o más de A
        devolveremos verdadero
         */
        if (Math.abs(a - b) >= 10 || Math.abs(b - c) >= 10
                || Math.abs(c - a) >= 10) {
            return true;
        } /*
        En cualquier otro caso, devolveremos falso
         */ else {
            return false;
        }
    }

    /*
    Ejercicio 24 - CodingBat Logic1 - withoutDoubles
    Devuelve la suma de dos dados de 6 caras, cada uno
    con números del 1 al 6. Si noDoubles es verdadero y
    los dos dados muestran el mismo número debemos devolver
    la suma +1, a no ser que los dos dados muestren el 6
    en cuyo caso debemos devolver el 7.
    
     */
    public int withoutDoubles(int die1, int die2, boolean noDoubles) {
        /*
        Si los dos dados muestran el 6 y noDoubles
        es verdadero, debemos devolver el 7
         */
        if (noDoubles && die1 == 6 && die2 == 6) {
            return 7;
        }
        /*
        Si noDoubles es verdadero y los dos dados
        muestran el mismo valor a excepción del 6,
        debemos devolver la suma de los números
        y añadirle 1.
         */
        if (noDoubles && die1 == die2 && die1 != 6 && die2 != 6) {
            return die1 + 1 + die2;
        }
        /*
        Si noDoubles es verdadero pero los dados tienen
        números distintos debemos devolver la suma
         */
        if (noDoubles && die1 != die2) {
            return die1 + die2;
        } /*
        Para el resto de casos, devolvemos simplemente
        la suma de ambos números. Sin incrementos ni nada
         */ else {
            return die1 + die2;
        }
    }

    /*
    Ejercicio 25 - CodingBat Logic1 - maxMod5
    Dados dos enteros, devuelve el valor que es mayor.
    Si los dos valores son iguales, devuelve 0.
    Si los dos números tienen el mismo resto al realizar
    la división entera (%) entre 5, devolvemos el menor
    en lugar de devolver el mayor.
     */
    public int maxMod5(int a, int b) {
        /*
        Si los números son iguales, devolveremos 0
         */
        if (a == b) {
            return 0;
        }
        /*
        Si los números son distintos y su división
        entera (%) entre 5 también y A menor que B
        devolveremos el mayor, en este caso B
         */
        if (a != b && a % 5 != b % 5 && a < b) {
            return b;
        }
        /*
        Pero en el caso de que la división entera
        entre 5 sea igual siendo A y B distintos,
        Si B es menor que A devolveremos B
         */
        if (a != b && a % 5 == b % 5 && b < a) {
            return b;
        } /*
        Para cualquier otro caso, devolveremos A
         */ else {
            return a;
        }

    }

    /*
    Ejercicio 26 - CodingBat Logic1 - redTicket
    Tienes un ticket rojo de la lotería que muestra
    tres números, 0, 1 o 2. Si los tres números son
    doses (2) devolverá un 10, de todas maneras si
    son iguales, devolverá un 5 y si B o C difieren
    de A, el programa devolverá un 1. Si no un 0.
     */
    public int redTicket(int a, int b, int c) {
        /*
        Si A, B y C son iguales y además son iguales
        al número 2, el programa devolverá un 10
         */
        if (a == 2 && b == 2 && c == 2) {
            return 10;
        }
        /*
        Si simplemente son iguales devolverá un 5
         */
        if (a == b && b == c) {
            return 5;
        }
        /*
        Si se da el caso de que B y C difieren de A
        pero son iguales B y C devolverá un 1
         */
        if (a != b && c != a) {
            return 1;
        } /*
        Para culaquier otro caso, devolveremos un 0
         */ else {
            return 0;
        }
    }

    /*
    Ejercicio 27 - CodingBat Logic1 - greenTicket
    Tienes un ticket verde de lotería con tres enteros
    A, B y C. Si los números son todos diferentes, 
    devolverá un 0. Si los tres son iguales un 20 y si
    al menos, dos son iguales un 10.
     */
    public int greenTicket(int a, int b, int c) {
        /*
        Si A, B y C son iguales, devolverá un 20
         */
        if (a == b && b == c) {
            return 20;
        }
        /*
        Si bien A y B son iguales, O B y C son iguales
        o bien C y A son iguales devolverá un 10
         */
        if (a == b && c != b || b == c && c != a || c == a && b != a) {
            return 10;
        } /*
        En cualquier otro caso, devolveremos un 0
         */ else {
            return 0;
        }
    }

    /*
    Ejercicio 28 - CodingBat Logic1 - blueTicket
    Tienes un ticket azul de lotería con tres enteros
    A, B y C en él. Esos números hacen tres parejas,
    que son AB, BC y BA. Considerando sólamente estas
    parejas, si alguna de ellas suma exactamente 10,
    devolveremos un 10. De otro modo, si AB suma exactamente
    10 más que BC o AC devolverá un 0. Si no se da 
    ninguno de estos casos, devolveremos un 0.
     */
    public int blueTicket(int a, int b, int c) {
        /*
        Si alguna de las parejas es igual a 10, devolvemos 10
         */
        if (a + b == 10 || b + c == 10 || c + a == 10) {
            return 10;
        }
        /*
        Si AB suma 10 más que BC o AC devolvemos 5
         */
        if (a + b == 10 + (b + c) || a + b == 10 + (a + c)) {
            return 5;
        } /*
        En cualquier otro caso, devolvemos 0
         */ else {
            return 0;
        }
    }

    /*
    Ejercicio 29 - CodingBat Logic1 - shareDigit
    Dados dos enteros, ambos entre 10 y 99 devuelve
    verdadero si al menos hay un dígito que aparezca
    en ambos.
     */
    public boolean shareDigit(int a, int b) {
        /*
        Comparamos el dígito izquierdo de A (/)
        con el dígito derecho de B (%) y el dígito
        derecho de A (%) con el digito izuqierdo de B(/)
        
        Del mismo modo comparamos los dígitos izquierdos
        de ambos números (/) y los dígitos derechos (%)
        
        Si alguna de estas operaciones coincide, devolveremos
        verdadero.
         */
        if (a / 10 == b % 10 || a % 10 == b / 10
                || a % 10 == b % 10 || a / 10 == b / 10) {
            return true;
        }
        /*
        Esta sentencia puede ser redundante, pero ayuda a 
        entender un caso. Si A y B son iguales, obviamente
        los dos tienen los mismos dígitos y en la misma
        posición y por tanto, devolvermeos verdadero.
         */
        if (a == b) {
            return true;
        } /*
        En cualqueir otro caso, devolveremos false
         */ else {
            return false;
        }
    }

    /*
    Ejercicio 30 - CodingBat Logic1 - sumLimit
    Dados 2 enteros no negtivos A y B devolveremos su
    suma si esta tiene el mismo número de digitos que A
    Si la suma tiene más dígitos que A devolveremos A.
    
    Hay que comparar la longitud de los números.
    Para ello hay que pasar el entero a un String.
     */
    public int sumLimit(int a, int b) {
        /*
        Declaramos un String para pasar el valor
        de A y de la Suma a un String y luego
        poder medir su longitud mediante (.length())
         */
        String letraA = String.valueOf(a);
        String suma = String.valueOf(a + b);
        /*
        Si la longitud de A y de la suma son iguales
        devolveremos la suma.
         */
        if (letraA.length() == suma.length()) {
            return a + b;
        } /*
        Si no se cumple eso, devolveremos
        sólamente A sin B.
         */ else {
            return a;
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
}
